package com.humxa.networking.api

import com.humxa.networking.BuildConfig
import com.humxa.networking.data.SearchResultResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/3/search/multi")
    suspend fun getSearchResults(
        @Query("query") searchQuery:String,
        @Query("page") page:Int = 1,
        @Query("language") language:String = "en-US",
        @Query("include_adult") adult:Boolean= false,
        @Query("api_key") apiKey:String = BuildConfig.API_KEY
    ):SearchResultResponse
}
package com.humxa.networking.data

class ForbiddenException(message: String = "Access Denied") : Exception(message)
class ServerException(message: String = "Server error") : Exception(message)
class NotFoundException(message: String = "") : Exception(message)
class InvalidOtpException(message: String = "Invalid Code") : Exception(message)
class UnauthorizedException(message: String = "Unauthorized") : Exception(message)

class TimeoutConnectionException(message: String?) : Exception(message)
class UnexpectedException(message: String?) : Exception(message)
class ClientConnectionException(message: String?) : Exception(message)
class ResponseErrorException(message: String) : Exception(message)
class NullDataException(message: String) : Exception(message)
class BasketConflictException(message: String) : Exception(message)
class ExpectationFailed(message: String) : Exception(message)
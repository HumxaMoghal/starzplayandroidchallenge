package com.humxa.networking.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class SearchResultResponse(

    @SerializedName("page")
    val page: Int = 1,

    @SerializedName("results")
    val results: List<Movie> = listOf(),

    @SerializedName("total_pages")
    val totalPages: Int = 1,

    @SerializedName("total_results")
    val totalResults: Int = 0

)

@Parcelize
data class Movie(

    @SerializedName("adult")
    val isAdult: Boolean? = false,

    @SerializedName("backdrop_path")
    val backDropPath: String? = null,

    @SerializedName("genre_ids")
    val genreIds: List<Int>? = null,

    @SerializedName("id")
    val id: Long,

    @SerializedName("media_type")
    val mediaType: String,

    @SerializedName("original_language")
    val originalLanguage: String? = null,

    @SerializedName("original_title")
    val originalTitle: String? = null,

    @SerializedName("overview")
    val overView: String? = null,

    @SerializedName("popularity")
    val popularity: Float = 0F,

    @SerializedName("poster_path")
    val posterPath: String? = null,

    @SerializedName("release_date")
    val releaseDate: String? = null,

    @SerializedName("title")
    val title: String? = null,

    @SerializedName("video")
    val isVideo: Boolean = false,

    @SerializedName("vote_average")
    val voteAverage: Float = 0F,

    @SerializedName("vote_count")
    val voteCount: Int = 0

) : Parcelable
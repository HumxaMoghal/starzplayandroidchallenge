package com.humxa.networking.repo

import com.humxa.networking.data.SearchResultResponse
import kotlinx.coroutines.flow.Flow

interface ISearchRepository {
    fun searchResult(query:String,page:Int):Flow<SearchResultResponse>
}
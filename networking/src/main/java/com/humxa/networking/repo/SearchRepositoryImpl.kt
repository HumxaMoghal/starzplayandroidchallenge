package com.humxa.networking.repo

import com.humxa.networking.api.ApiService
import com.humxa.networking.data.SearchResultResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchRepositoryImpl @Inject constructor(private val apiService: ApiService) :
    ISearchRepository {

    override fun searchResult(query: String, page: Int): Flow<SearchResultResponse> = flow {
        val response = apiService.getSearchResults(query,page)
        emit(response)
    }.flowOn(Dispatchers.IO)

}
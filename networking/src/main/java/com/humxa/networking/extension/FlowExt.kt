package com.humxa.networking.extension

import com.google.gson.JsonSyntaxException
import com.humxa.networking.data.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.catch
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.SSLException


fun<T> Flow<T>.onError(
    block: FlowCollector<T>.(Throwable) -> Unit
) = catch {
       handleException(it, this, block)
}


suspend inline fun<T> Flow<T>.onSuccess(
    crossinline block: (value: T) -> Unit
) = collect {
    block(it)
}

fun<T> handleException(
    throwable: Throwable,
    flowCollector: FlowCollector<T>,
    block: FlowCollector<T>.(Throwable) -> Unit
) {

   val exception =  when (throwable) {
            is ResponseErrorException -> {
                throwable.also { block(flowCollector,it) }
            }
            is HttpException -> {

                val errorResponse = throwable.response()?.errorBody()?.string() ?: throwable.message ?: ""

                when (throwable.code()) {
                    401 -> UnauthorizedException(errorResponse)
                    403 -> ForbiddenException(errorResponse)
                    404 -> NotFoundException(errorResponse)
                    417 -> ExpectationFailed(errorResponse).also { block.invoke(flowCollector, it) }
                    500 -> ServerException(errorResponse)
                    else -> {
                        try {
                            // val response = Gson().fromJson(errorResponse, NetworkErrorResponse::class.java)
                            block(flowCollector,throwable)
                            throwable

                        } catch (e: JsonSyntaxException) {
                            UnexpectedException(errorResponse)
                        }
                    }

                }.also {
                    if(it != throwable)
                        block(flowCollector, it)
                }
            }
            is ConnectException,
            is UnknownHostException,
            is SSLException -> {
                ClientConnectionException(throwable.message).also { block(flowCollector,it) }
            }
            is SocketTimeoutException -> {
                TimeoutConnectionException(throwable.message).also{ block.invoke(flowCollector, it) }
            }
            is NullDataException ->
                throwable.also {
                    block(flowCollector, it)
                }
            else -> {
                UnexpectedException(throwable.message).also {
                    block(flowCollector, it)
                }
            }
        }
    exception.printStackTrace()
}

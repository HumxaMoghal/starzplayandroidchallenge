package com.humxa.android

import com.humxa.networking.api.ApiService
import com.humxa.networking.data.Movie
import com.humxa.networking.data.SearchResultResponse
import com.humxa.networking.repo.ISearchRepository
import com.humxa.networking.repo.SearchRepositoryImpl
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test


class SearchRepositoryTest {
    lateinit var searchRepository: ISearchRepository
    lateinit var apiService: ApiService

    @Before
    fun setup() {
        apiService = mockk()
        searchRepository = SearchRepositoryImpl(apiService)
    }

    @Test
    fun whenSearchResultIsPresent() {
        coEvery { apiService.getSearchResults(any()) }.returns(
            SearchResultResponse(
                1,
                listOf(Movie(id = 1, mediaType = "movie"))
            )
        )

        runBlocking {
            val result = searchRepository.searchResult("", 1)
            assert(result.toList().isNotEmpty())
        }
    }

}
package com.humxa.android.feature.main.data

sealed class ScreenState {
    object NONE : ScreenState()
    object EMPTY : ScreenState()
    object LOADING : ScreenState()
    object SUCCESS : ScreenState()
    data class ERROR(val msg: String?) : ScreenState()
}

package com.humxa.android.feature.main.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.humxa.android.databinding.FragmentPlayerBinding
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSource

class PlayerFragment : Fragment() {

    companion object {
        private const val STREAM_URL = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
    }

    private lateinit var binding: FragmentPlayerBinding
    // private val args by navArgs<PlayerFragmentArgs>()
    private lateinit var exoPlayer: ExoPlayer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlayerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        setUpPlayer()
    }

    private fun setUpView() {
        // binding.ivBanner.loadImage("https://image.tmdb.org/t/p/w500${args.imageUrl}")
    }

    private fun setUpPlayer() {
        val dataFactory = DefaultDataSource.Factory(requireActivity())
        val mediaSource = ProgressiveMediaSource.Factory(dataFactory)
            .createMediaSource(MediaItem.fromUri(STREAM_URL))
        val mediaSourceFactory = DefaultMediaSourceFactory(dataFactory)
        exoPlayer = ExoPlayer.Builder(requireActivity())
            .setMediaSourceFactory(mediaSourceFactory)
            .build()
        exoPlayer.addMediaSource(mediaSource)
        exoPlayer.playWhenReady = true
        binding.playerView.player = exoPlayer
        binding.playerView.requestFocus()
        exoPlayer.play()
    }

    private fun releasePlayer() {
        exoPlayer.release()
    }

    override fun onDestroyView() {
        releasePlayer()
        super.onDestroyView()
    }

}
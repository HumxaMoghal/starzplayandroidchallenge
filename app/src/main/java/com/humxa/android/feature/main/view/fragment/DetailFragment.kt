package com.humxa.android.feature.main.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.humxa.android.databinding.FragmentDetailBinding
import com.humxa.android.loadImage
import com.humxa.android.feature.main.viewmodel.MainViewModel

class DetailFragment : Fragment() {

    private lateinit var binding:FragmentDetailBinding
    private val viewModel: MainViewModel by activityViewModels()
    private val args:DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews(){
        args.movie?.let { movie ->
            binding.tvTitle.text = movie.title
            binding.tvDesc.text = movie.overView
            binding.ivBanner.loadImage("https://image.tmdb.org/t/p/w500${movie.backDropPath}")
            binding.btnPlay.setOnClickListener {
                viewModel.onClickPlay(movie)
            }
        }
        binding.btnBack.setOnClickListener { activity?.onBackPressed() }
    }

}
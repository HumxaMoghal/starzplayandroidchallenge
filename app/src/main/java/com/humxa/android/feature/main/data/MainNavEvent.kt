package com.humxa.android.feature.main.data

import com.humxa.networking.data.Movie

sealed class MainNavEvent {
    data class ToDetail(val movie: Movie) : MainNavEvent()
    data class ToPlay(val imageUrl:String) : MainNavEvent()
}

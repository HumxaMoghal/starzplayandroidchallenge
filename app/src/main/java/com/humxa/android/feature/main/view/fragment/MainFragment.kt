package com.humxa.android.feature.main.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.humxa.android.core.DebouncingQueryTextListener
import com.humxa.android.databinding.FragmentMainBinding
import com.humxa.android.feature.main.adapter.MainAdapter
import com.humxa.android.feature.main.viewmodel.MainViewModel
import com.humxa.networking.data.Movie
import kotlinx.coroutines.flow.collectLatest

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var adapter: MainAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (this::binding.isInitialized) return binding.root
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        observeMovies()
    }

    private fun setUpViews() {
        binding.etSearch.isFocusable = true
        binding.etSearch.isIconified = false
        binding.etSearch.requestFocusFromTouch()
        binding.etSearch.setOnQueryTextListener(DebouncingQueryTextListener(viewLifecycleOwner.lifecycle) { newText ->
            newText?.let {
                viewModel.resetLastSearchData()
                if (it.isEmpty()) {
                    viewModel.resetSearch()
                } else {
                    viewModel.searchMovie(it)
                }
            }
        })
    }

    private fun observeMovies() = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
        viewModel.movies.collectLatest { movies ->
            if (this@MainFragment::adapter.isInitialized) {
                if (viewModel.isNextPage()) {
                    adapter.getData().forEachIndexed { index, wrapper ->
                        movies.find { it.title == wrapper.title }?.let { found ->
                            wrapper.movies.addAll(found.movies)
                            (binding.rvMain.findViewHolderForAdapterPosition(index) as? MainAdapter.ViewHolder?)?.updateData(
                                found.movies
                            )
                        }

                    }
                }
                else adapter.updateList(movies)
            } else {
                binding.rvMain.layoutManager = LinearLayoutManager(requireContext())
                adapter = MainAdapter(
                    movies.toMutableList(),
                    this@MainFragment::onMovieClick,
                    this@MainFragment::requestLoadNextPage
                )
                binding.rvMain.adapter = adapter
            }
        }
    }

    private fun onMovieClick(movie: Movie) = viewModel.onMovieClick(movie)

    private fun requestLoadNextPage() = viewModel.requestLoadNextPage()

}
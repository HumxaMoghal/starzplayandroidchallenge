package com.humxa.android.feature.main.data

import com.humxa.networking.data.Movie

data class MainAdapterWrapper(val title:String,val movies:MutableList<Movie>)

package com.humxa.android.feature.main.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.humxa.android.R
import com.humxa.android.databinding.ItemMainBinding
import com.humxa.android.feature.main.data.MainAdapterWrapper
import com.humxa.networking.data.Movie

class MainAdapter(
    private var data: List<MainAdapterWrapper> = mutableListOf(),
    private val onMovieClick: ((Movie) -> Unit)? = null,
    private val requestLoadNextPage: (() -> Unit)? = null
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val viewPool by lazy { RecyclerView.RecycledViewPool() }

    fun getData() = data

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(list: List<MainAdapterWrapper>) {
        data = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_main,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data[position].let {
            holder.bind(title = it.title, it.movies)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val binding = ItemMainBinding.bind(view)
        private val lastVisiblePos: Int
            get() = (binding.rvMovies.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()

        private var movies = mutableListOf<Movie>()
        private lateinit var adapter: MoviesAdapter

        fun updateData(list: List<Movie>) {
             val startIndex = movies.size
             movies.addAll(list)
             adapter.notifyItemRangeChanged(startIndex, movies.lastIndex)
        }

        fun bind(title: String, list: List<Movie>) {
            movies = list.toMutableList()
            binding.rvMovies.setRecycledViewPool(viewPool)
            binding.tvTitle.text = title
            binding.rvMovies.layoutManager = LinearLayoutManager(
                binding.rvMovies.context,
                RecyclerView.HORIZONTAL,
                false
            )
            binding.rvMovies.addOnScrollListener(scrollChangeListener)
            adapter = MoviesAdapter(movies, onMovieClick)
            binding.rvMovies.adapter = adapter
        }

        private val scrollChangeListener by lazy {
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        Log.e("SCROLLED", "$lastVisiblePos")
                        if (lastVisiblePos + 1 >= (binding.rvMovies.adapter?.itemCount ?: 0)) {
                            requestLoadNextPage?.invoke()
                        }
                    }
                }
            }
        }

    }
}
package com.humxa.android.feature.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.humxa.android.feature.main.data.MainAdapterWrapper
import com.humxa.android.feature.main.data.MainNavEvent
import com.humxa.networking.data.Movie
import com.humxa.android.feature.main.data.ScreenState
import com.humxa.networking.extension.onError
import com.humxa.networking.extension.onSuccess
import com.humxa.networking.repo.ISearchRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repo: ISearchRepository) : ViewModel() {

    private val _screenState:MutableLiveData<ScreenState> = MutableLiveData()
    val screenState:LiveData<ScreenState>
    get() = _screenState

    private val _movies:MutableSharedFlow<List<MainAdapterWrapper>> = MutableSharedFlow()
    val movies:SharedFlow<List<MainAdapterWrapper>>
    get() = _movies.asSharedFlow()

    private val _navigate : MutableSharedFlow<MainNavEvent> = MutableSharedFlow()
    val navigate: SharedFlow<MainNavEvent>
    get() = _navigate.asSharedFlow()

    private var page = 1
    private var noMorePage = false
    private var lastQuery = ""

    fun isNextPage() = page > 2

    fun resetLastSearchData(){
        page = 1
        noMorePage = false
        lastQuery = ""
    }

    fun searchMovie(query:String) = viewModelScope.launch {
        repo.searchResult(query,page)
            .onStart {
                if(isNextPage().not())  _screenState.value = ScreenState.LOADING
            }
            .onError { exception ->
                _screenState.value = ScreenState.ERROR(exception.message)
            }
            .onSuccess { response ->
                lastQuery = query
                if(response.totalPages > page) { page +=1 }
                else noMorePage = true
                _screenState.value = ScreenState.SUCCESS
                viewModelScope.launch(Dispatchers.Default) {
                    val filtered = response.results
                    val data = filtered.groupBy { it.mediaType }.map { entry -> MainAdapterWrapper(
                        entry.key.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() },
                        entry.value.toMutableList()
                    ) }.sortedBy { it.title }
                    _movies.emit(data)
                }
            }
    }

    fun resetSearch() {
        viewModelScope.launch { _movies.emit(listOf()) }
    }

    fun onMovieClick(movie: Movie) = viewModelScope.launch {
        _navigate.emit(MainNavEvent.ToDetail(movie))
    }

    fun onClickPlay(movie: Movie) = viewModelScope.launch {
        movie.backDropPath?.let { _navigate.emit(MainNavEvent.ToPlay(it)) }
    }

    fun requestLoadNextPage() {
       if(noMorePage.not()) searchMovie(lastQuery)
    }

}
package com.humxa.android.feature.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.humxa.android.R
import com.humxa.android.databinding.ItemMovieBinding
import com.humxa.android.loadImage
import com.humxa.networking.data.Movie

class MoviesAdapter(
    private val movies: List<Movie>,
    private val onMovieClick:((Movie) -> Unit)? = null) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_movie,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(item = movies[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val binding = ItemMovieBinding.bind(view)

        fun bind(item: Movie) {
            binding.ivCarousel.loadImage("https://image.tmdb.org/t/p/w500${item.posterPath}")
            binding.root.setOnClickListener {
                onMovieClick?.invoke(item)
            }
        }
    }
}
package com.humxa.android.feature.main.view.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import com.humxa.android.R
import com.humxa.android.databinding.ActivityMainBinding
import com.humxa.android.feature.main.data.MainNavEvent
import com.humxa.android.feature.main.data.ScreenState
import com.humxa.android.feature.main.view.fragment.DetailFragmentArgs
import com.humxa.android.feature.main.view.fragment.PlayerFragmentArgs
import com.humxa.android.feature.main.viewmodel.MainViewModel
import com.humxa.networking.data.Movie
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding
    private lateinit var navController: NavController
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViews()
        setUpObservers()
    }

    private fun setUpViews(){
        setUpNavigation()
    }

    private fun setUpNavigation() {
        navController = (supportFragmentManager.findFragmentById(R.id.main_container) as NavHostFragment).navController
        val navGraph = navController.navInflater.inflate(R.navigation.nav_main)
        navController.graph = navGraph
        navController.addOnDestinationChangedListener{ _,destination,_ ->
            onNavDestinationChange(destination)
        }
    }

    private fun onNavDestinationChange(destination: NavDestination) {
        when(destination.id){
            R.id.playerFragment ->{
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                WindowCompat.getInsetsController(window, window.decorView).let {
                    it.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    it.hide(WindowInsetsCompat.Type.systemBars())
                }
            }
            else ->{
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
                WindowCompat.getInsetsController(window, window.decorView).show(WindowInsetsCompat.Type.systemBars())
            }
        }
    }

    private fun setUpObservers(){
        observerScreenState()
        observeNavigation()
    }

    private fun observerScreenState() = viewModel.screenState.observe(this) { screenState ->
        when(screenState){
            ScreenState.EMPTY -> {
                binding.progressBar.isVisible = false
            }
            is ScreenState.ERROR -> {
                binding.progressBar.isVisible = false
            }
            ScreenState.LOADING -> binding.progressBar.isVisible = true
            ScreenState.NONE -> {
                binding.progressBar.isVisible = false
            }
            ScreenState.SUCCESS -> {
                binding.progressBar.isVisible = false
            }
        }
    }

    private fun observeNavigation() = lifecycleScope.launchWhenStarted {
        viewModel.navigate.collectLatest { navigate ->
            when(navigate){
                is MainNavEvent.ToDetail -> navigateToDetails(navigate.movie)
                is MainNavEvent.ToPlay -> navigateToPlay(navigate.imageUrl)
            }
        }
    }

    private fun navigateToDetails(movie: Movie) {
        try {
            val args = DetailFragmentArgs(movie)
            navController.navigate(R.id.action_mainFragment_to_detailFragment,args.toBundle())
        }
        catch(e:Exception){
            e.printStackTrace()
        }
    }

    private fun navigateToPlay(imageUrl: String) {
        val args = PlayerFragmentArgs(imageUrl)
        navController.navigate(R.id.action_detailFragment_to_playerFragment,args.toBundle())
    }

}